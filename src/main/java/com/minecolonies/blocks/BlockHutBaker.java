package com.minecolonies.blocks;

public class BlockHutBaker extends BlockHut
{
    protected BlockHutBaker()
    {
        super();
    }

    @Override
    public String getName()
    {
        return "blockHutBaker";
    }
}