package com.minecolonies.blocks;

public class BlockHutCitizen extends BlockHut
{
    public BlockHutCitizen()
    {
        super();
    }

    @Override
    public String getName()
    {
        return "blockHutCitizen";
    }
}
