package com.minecolonies.blocks;

public class BlockHutBlacksmith extends BlockHut
{
    protected BlockHutBlacksmith()
    {
        super();
    }

    @Override
    public String getName()
    {
        return "blockHutBlacksmith";
    }
}