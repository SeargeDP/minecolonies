package com.minecolonies.blocks;

public class BlockHutMiner extends BlockHut
{
    protected BlockHutMiner()
    {
        super();
    }

    @Override
    public String getName()
    {
        return "blockHutMiner";
    }
}
