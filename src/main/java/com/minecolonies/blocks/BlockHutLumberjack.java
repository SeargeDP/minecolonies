package com.minecolonies.blocks;

public class BlockHutLumberjack extends BlockHut
{
    protected BlockHutLumberjack()
    {
        super();
    }

    @Override
    public String getName()
    {
        return "blockHutLumberjack";
    }
}
