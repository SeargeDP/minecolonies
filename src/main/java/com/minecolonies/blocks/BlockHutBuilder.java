package com.minecolonies.blocks;

public class BlockHutBuilder extends BlockHut
{
    protected BlockHutBuilder()
    {
        super();
    }

    @Override
    public String getName()
    {
        return "blockHutBuilder";
    }
}
